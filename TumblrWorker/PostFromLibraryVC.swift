//
//  PostFromLibraryVC.swift
//  TumblrWorker
//
//  Created by Eugene Liashenko on 10/27/15.
//  Copyright © 2015 development. All rights reserved.
//

import UIKit
import Photos
import MBProgressHUD


class PostFromLibraryVC :UIViewController, UITableViewDataSource, UITableViewDelegate, UIImagePickerControllerDelegate,
UINavigationControllerDelegate {
    
    @IBOutlet weak var tagsTV : UITextView!
    @IBOutlet weak var postBtn: UIButton!
    
    var imageData :Data?

    let tags = ["girl,nice,women,hot,inspiration,fashion,ero,sexy,sport,fitness,butt,ass,tits",
                "girl,nice,women,hot,inspiration,fashion,ero,sexy,sport,fitness,butt,ass"]

    override func viewDidLoad() {

        super.viewDidLoad()

        let picker = UIImagePickerController()

        picker.delegate      = self
        picker.allowsEditing = false
        picker.sourceType    = .photoLibrary
        present(picker, animated: true, completion: nil)
        
        postBtn.isEnabled = false
    }
    
    @IBAction func onPost(_ sender: AnyObject) {
        
        guard let imageData = imageData else {return}

        UIRouter.makePhotoPostWithData(imageData, inViewController: self, tags: tagsTV.text)
    }

    //MARK:- UITableViewDataSource
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return tags.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "RecentlyTagsTableCell", for: indexPath) 
        
        cell.textLabel?.text = tags[indexPath.row]
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {

        tagsTV.text = tags[indexPath.row]
    }
    
    //MARK:-
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {

        dismiss(animated: true, completion: nil)

        if let im = info[UIImagePickerControllerOriginalImage] as? UIImage,
            let imageData = UIImageJPEGRepresentation(im, 1)
        {
            self.imageData = imageData
            
            if let imageURL = info[UIImagePickerControllerReferenceURL] as? URL {
                
                deleteImageAtUrl(imageURL)
            }

            self.onPost(self)

            postBtn.isEnabled = true
        }
    }
    
    func deleteImageAtUrl(_ url :URL) {
        
        PHPhotoLibrary.shared().performChanges( {
            
            let imageAssetToDelete = PHAsset.fetchAssets(withALAssetURLs: [url], options: nil)
            
            PHAssetChangeRequest.deleteAssets(imageAssetToDelete)
        },
        completionHandler: { success, error in

            print("Finished deleting asset.")
        })
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        
        
    }


}
