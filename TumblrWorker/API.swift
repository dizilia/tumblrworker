//
//  API.swift
//  TumblrWorker
//
//  Created by Eugene Liashenko on 10/16/15.
//  Copyright © 2015 development. All rights reserved.
//

import Foundation
import OAuthSwift
import Haneke

class API {

    var baseUrl = "https://api.tumblr.com/v2/"

    lazy var oauthsController :OAuth1Swift = {
       
        let oauthswift = OAuth1Swift(consumerKey: Keys.OAuthConsumerKey,
                                  consumerSecret: Keys.OAuthConsumerSecret,
                                 requestTokenUrl: "https://www.tumblr.com/oauth/request_token",
                                    authorizeUrl: "https://www.tumblr.com/oauth/authorize",
                                  accessTokenUrl: "https://www.tumblr.com/oauth/access_token"
        )

        oauthswift.client.credential.oauthToken       = Keys.OAuthToken
        oauthswift.client.credential.oauthTokenSecret = Keys.OAuthTokenSecret

        return oauthswift
    }()

    func getUserInfo(_ callBack:((NetworkOperationResult<Info>)->Void)?) {

        oauthsController.client.get("https://api.tumblr.com/v2/user/info", success: { data, response in

            if let json = try! JSONSerialization.jsonObject(with: data, options: JSONSerialization.ReadingOptions.allowFragments) as? Dictionary<String, AnyObject> {
                
                let parser = InfoParser(json: json)

                callBack?(NetworkOperationResult<Info>.done(parser.parse()))

            }
            else {
                callBack?(NetworkOperationResult<Info>.error)
            }
        }
        , failure: { error in

            print(error)
        })
    }

    func getPostsForBlogName(_ blogName  : String,
                                type   : PostType? = .none,
                                params : PostRequestParameters? = .none,
                              callBack : ((NetworkOperationResult<[Post]>)->Void)?)
    {
        let path = "blog/\(blogName)/posts" + ((type != nil) ? "/\(type!.rawValue)" : "") + "?"

        var paramsDict = Dictionary<String,String>()

        params?.dictionary().forEach({

            paramsDict.updateValue($1, forKey: $0)
        })

        let urlString = Helpers.buildUrlString(baseUrl, urlString: path, parameters: paramsDict)

        oauthsController.client.get(urlString, success: { data, response in

            PostsParserOperation(data: data, didFinish: callBack).parse()

        },failure: { error in

            print(error)
        })
    }

    func postToBlog(_ name :String, params :PostCreateParameters, dataParams :[String:Data]? = .none, callBack : ((NetworkOperationResult<Bool>)->Void)?) {

        let path = "blog/\(name)/post"

        var paramsDict = [String: String]()

        params.dictionary().forEach({

            paramsDict.updateValue($1, forKey: $0)
        })

        paramsDict["api_key"] = Keys.OAuthConsumerKey

        let urlString = Helpers.buildUrlString(baseUrl, urlString: path, parameters: [String:String]())

        let requestOp = PostRequestOperation(requestUrlString: urlString, params: paramsDict, dataParams: dataParams)

        let _ = NetworkWorkFlow(requestOp: requestOp, parserOp: PostParserOperation(didFinish: callBack))
    }
    
    func likePost(postId :Int, reblogKey :String) {

        var params = OAuthSwift.Parameters()

        params["id"]         = postId
        params["reblog_key"] = reblogKey
        
        oauthsController.client.post(baseUrl + "user/like", parameters: params, success: { data, response in
            
        }, failure: { error in
                
            print(error)
        })
    }
}
