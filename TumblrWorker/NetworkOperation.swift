//
//  NetworkOperation.swift
//  TumblrWorker
//
//  Created by Eugene Liashenko on 10/16/15.
//  Copyright © 2015 development. All rights reserved.
//

import Foundation
import MobileCoreServices

class NetworkWorkFlow :DidFinishDownloadPt{
    
    var operationQueue = OperationQueue()

    var parser   : ParserOperation
    var requestOp: RequestOperation

    init(requestOp: RequestOperation, parserOp: ParserOperation) {
        
        self.requestOp = requestOp
        self.parser    = parserOp

        self.requestOp.delegate = self

        configureOperationQueue()

        operationQueue.addOperation(self.requestOp)
    }
    
    func downloadOperation(_ operation: Operation, didFinishDownloadingWithResult result: DownloadOperationResult) {

        parser.json = result.json

        operationQueue.addOperation(parser)
    }

    fileprivate func configureOperationQueue() {

        operationQueue = OperationQueue()
        operationQueue.maxConcurrentOperationCount = 1
        operationQueue.qualityOfService            = .userInitiated
        operationQueue.name                        = "quotes.list.queue"
    }
}

class ParserOperation :Operation {
    
    var json :Dictionary<String, AnyObject>!

    override func main() {

        parse()
    }

    func parse() {

    }
}

class RequestOperation :Operation, NSURLConnectionDelegate, NSURLConnectionDataDelegate {
    
    var method: RequestMethod = .GET

    var delegate: DidFinishDownloadPt?
    
    fileprivate var connection: NSURLConnection?
    fileprivate var request   : NSMutableURLRequest!

    fileprivate var _executing = false
    fileprivate var _finished  = false

    override func main() {
        
        if isCancelled == true && _finished != false {

            return
        }
        
        httpGet(request as URLRequest!, callback: handleResponceData)
    }

    func httpGet(_ request: URLRequest!, callback: @escaping (Data?, Error?) -> Void) {

        let session = URLSession.shared

        let task = session.dataTask(with: request, completionHandler: { (data, response, error) -> Void in

            callback(data, error)
        })
        task.resume()
    }

    override var isFinished: Bool {

        return _finished
    }

    func handleResponceData(_ data :Data?, error :Error?) {
        
        guard let data = data else {
            
            print("ERROR :handleResponceData \(error)")
            return
        }

        do {
            if let json = try JSONSerialization.jsonObject(with: data, options: JSONSerialization.ReadingOptions.allowFragments) as? Dictionary<String, AnyObject> {
                
                delegate?.downloadOperation(self, didFinishDownloadingWithResult: (true, json))
            }
            else {
                
                delegate?.downloadOperation(self, didFinishDownloadingWithResult: (true, nil))
            }
            
        } catch {
            print("Exeption")
            
            NSLog("%@", NSString(data: data, encoding: String.Encoding.utf8.rawValue)!)
        }
        
        finish()
        
        cancel()
    }

    func finish() {
        
        willChangeValue(forKey: "isExecuting")
        willChangeValue(forKey: "isFinished")

        _executing = false
        _finished  = true

        didChangeValue(forKey: "isExecuting")
        didChangeValue(forKey: "isFinished")
    }
}

class GetRequestOperation :RequestOperation {
    
    init(requestUrlString: String,
        startOnMainThread: Bool = false,
             finishInMain: Bool = true)
    {
        super.init()
            
        method  = .GET
        request =  NSMutableURLRequest(url: URL(string: requestUrlString)!)

        request.signWithKeys( Keys.OAuthConsumerKey,
              consumerSecret: Keys.OAuthConsumerSecret,
                       token: Keys.OAuthToken,
                 tokenSecret: Keys.OAuthTokenSecret,
                      method: method.rawValue,
              postParameters: nil)

        name = "GetRequestOperation"

        completionBlock = {

            print("Get DONE")
        }
    }
}

class PostRequestOperation :RequestOperation {

    init(requestUrlString: String,
                   params: [String:String]? = .none,
               dataParams: [String:Data]?,
        startOnMainThread: Bool = false,
             finishInMain: Bool = true)
    {

        super.init()

        method  = .POST

        request =  NSMutableURLRequest(url: URL(string: requestUrlString)!)

        request.httpMethod = method.rawValue

        request.signWithKeys( Keys.OAuthConsumerKey,
              consumerSecret: Keys.OAuthConsumerSecret,
                       token: Keys.OAuthToken,
                 tokenSecret: Keys.OAuthTokenSecret,
                      method: method.rawValue,
              postParameters: params)


        if let dataParams = dataParams {

            let boundary = generateBoundaryString()

            request.setValue("multipart/form-data; boundary=\(boundary)", forHTTPHeaderField: "Content-Type")

            request.httpBody = createBodyWithParameters(params, key: "data[0]", dataParams: dataParams, boundary: boundary)
        }
        else {

            request.setValue("application/x-www-form-urlencoded; charset=utf-8", forHTTPHeaderField: "Content-Type")

            var paramsStr = ""

            if let postParameters = params {
                
                paramsStr = postParameters.reduce("", {
                    
                    return String.urlParametersCombine($0, paramsPair: $1)
                })
            }

            let requestBody = NSString(string: paramsStr).data(using: String.Encoding.utf8.rawValue)

            request.httpBodyStream = InputStream(data: requestBody!)

            self.request.setValue("JXHTTP", forHTTPHeaderField: "User-Agent")

            self.request.setValue("\(requestBody!.count)", forHTTPHeaderField: "Content-Length")
        }

        self.name = "PostRequestOperation"

        self.completionBlock = {
            
            print("Post DONE")
        }
    }

    func createBodyWithParameters(_ parameters: [String: String]?, key: String, dataParams: [String:Data], boundary: String) -> Data {

        let body = NSMutableData()
        
        if parameters != nil {

            for (key, value) in parameters! {

                body.appendString("--\(boundary)\r\n")
                body.appendString("Content-Disposition: form-data; name=\"\(key)\"\r\n\r\n")
                body.appendString("\(value)\r\n")
            }
        }

        for (mimetype, data) in dataParams {

            body.appendString("--\(boundary)\r\n")
            body.appendString("Content-Disposition: form-data; name=\"\(key)\"; filename=\"\("Image")\"\r\n")
            body.appendString("Content-Type: \(mimetype)\r\n\r\n")
            body.append(data)
            body.appendString("\r\n")
        }
        
        body.appendString("--\(boundary)--\r\n")

        return body as Data
    }
    
    func generateBoundaryString() -> String {

        return "Boundary-\(UUID().uuidString)"
    }
    
    func mimeTypeForPath(_ path: String) -> String {

        let url = URL(fileURLWithPath: path)
        let pathExtension = url.pathExtension

        if let uti = UTTypeCreatePreferredIdentifierForTag(kUTTagClassFilenameExtension, pathExtension as NSString, nil)?.takeRetainedValue() {
            if let mimetype = UTTypeCopyPreferredTagWithClass(uti, kUTTagClassMIMEType)?.takeRetainedValue() {
                return mimetype as String
            }
        }

        return "application/octet-stream";
    }
   
}

//MARK:-
enum NetworkOperationResult<T> {
    
    case done(T)
    case error
}

enum RequestMethod :String {
    
    case GET
    case POST
}

typealias DownloadOperationResult = (success: Bool, json: Dictionary<String, AnyObject>?)

protocol DidFinishDownloadPt {
    
    func downloadOperation(_ operation: Operation, didFinishDownloadingWithResult result: DownloadOperationResult)
}
