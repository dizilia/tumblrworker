//
//  API+Post.swift
//  TumblrWorker
//
//  Created by Eugene Liashenko on 10/22/15.
//  Copyright © 2015 development. All rights reserved.
//

import Foundation

extension API {
    
    enum PostType :String {

        case photo
        case chat
        case link
        case text
        case audio
        case video
    }

    struct PostRequestParameters {
        
        var id          :Int?
        var tag         :String?
        var limit       :Int?
        var offset      :Int?
        var reblog_info :Bool?
        var notes_info  :Bool?
        
        func dictionary() -> [String:String] {
            
            var result = [String:String]()
            
            if let id_          = id          { result["id"]          = "\(id_)"          }
            if let tag_         = tag         { result["tag"]         = tag_              }
            if let limit_       = limit       { result["limit"]       = "\(limit_)"       }
            if let offset_      = offset      { result["offset"]      = "\(offset_)"      }
            if let reblog_info_ = reblog_info { result["reblog_info"] = reblog_info_ ? "YES" : "NO"}
            if let notes_info_  = notes_info  { result["notes_info"]  = notes_info_  ? "YES" : "NO"}

            return result
        }
    }
    //https://www.tumblr.com/docs/en/api/v2#posting
    struct PostCreateParameters {
        
        var type   :PostType?
        var state  :PostState?
        var tags   :String?
        //Text
        var title  :String?
        var body   :String? //Required
        //Photo
        var caption :String?
        var link    :String?
        var source  :String? //Required, either source or data or data64
        //Link Posts
        var url         :String? //Required
        var description :String?
        var thumbnail   :String?
        var excerpt     :String?
        var author      :String?
        //Audio Posts
        var external_url :String? //Required, either data
        //Video Posts
        var embed        :String? //Required, either embed or data
        
        
        func dictionary() -> [String:String] {
            
            var result = [String:String]()
            
            if let type_         = type         { result["type"]         = type_.rawValue      }
            if let state_        = state        { result["state"]        = state_.rawValue     }
            if let tags_         = tags         { result["tags"]         = tags_               }
            if let title_        = title        { result["title"]        = title_.urlencode()  }
            if let body_         = body         { result["body"]         = body_               }
            if let caption_      = caption      { result["caption"]      = caption_            }
            if let link_         = link         { result["link"]         = link_               }
            if let source_       = source       { result["source"]       = source_             }
            if let url_          = url          { result["url"]          = url_                }
            if let description_  = description  { result["description"]  = description_        }
            if let thumbnail_    = thumbnail    { result["thumbnail"]    = thumbnail_          }
            if let excerpt_      = excerpt      { result["excerpt"]      = excerpt_.urlencode()}
            if let author_       = author       { result["author"]       = author_.urlencode() }
            if let external_url_ = external_url { result["external_url"] = external_url_       }
            if let embed_        = embed        { result["embed_"]       = embed_              }

            return result
        }
        
        enum PostType :String {
            case text
            case photo
            case quote
            case link
            case chat
            case audio
            case video
        }
        
        enum PostState :String {
            case published
            case draft
            case queue
//            case private
        }
    }
    

}