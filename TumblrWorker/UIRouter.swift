//
//  UIRouter.swift
//  TumblrWorker
//
//  Created by Eugene Liashenko on 10/30/15.
//  Copyright © 2015 development. All rights reserved.
//

import UIKit

class UIRouter {
    
    static func makePhotoPostWithData(_ data :Data, inViewController vc :UIViewController, tags :String? = .none) {
        
        let postAction = PostUIAction()

        postAction.postPhotoWithData(data, inViewController: vc, tags: tags)
    }
    
    static func showBlogAnalizeWithParant(_ naviagtionVC :UINavigationController, blogName:String) {
        
        guard let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "AnalizerVC") as? AnalizerVC else {
            
            print("!!!")
            return
        }

        vc.currentBlogName = blogName

        naviagtionVC.pushViewController(vc, animated: true)
    }
}
