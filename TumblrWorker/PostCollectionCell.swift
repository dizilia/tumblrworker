//
//  PostCollectionCell.swift
//  TumblrWorker
//
//  Created by Eugene Liashenko on 11/6/15.
//  Copyright © 2015 development. All rights reserved.
//

import UIKit

class PostCollectionCell:UICollectionViewCell {
    
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var textLabel: UILabel!
    
}
