//
//  ViewController.swift
//  TumblrWorker
//
//  Created by Eugene Liashenko on 10/16/15.
//  Copyright © 2015 development. All rights reserved.
//

import UIKit
import OAuthSwift

class ViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {

    @IBOutlet weak var blog1Btn: UIButton!
    @IBOutlet weak var blog2Btn: UIButton!
    @IBOutlet weak var logTableView: UITableView!

    private var logs = Array<String>()

    override func viewDidLoad() {

        super.viewDidLoad()
        
        let base = Base()
//        
//        base.touchBlog("di-ste-fano.tumblr.com", date: NSDate())
//        base.touchBlog("dirty-hawke.tumblr.com", date: NSDate())
//        base.touchBlog("miss-tf.tumblr.com", date: NSDate())
//        base.touchBlog("luxuryandbeautiful.tumblr.com", date: NSDate())

        let api = API()
        
        api.getUserInfo(){[weak self] result in

            switch result {
            case let .done(info):

                self?.blog1Btn.setTitle("\(info.blogs.first?.followers)", for: UIControlState())
                self?.blog2Btn.setTitle("\(info.blogs.last?.followers)" , for: UIControlState())

                break
            case .error:
                break
            }
        }

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func onActionButton(_ sender: AnyObject) {
     
        
    }
    
    public func addLog(log :String) {
        
        logs.insert("\(Date()) \(log)", at: 0)

        logTableView.reloadData()
    }
    
    //MARK:- UITableViewDataSource
    func numberOfSections(in tableView: UITableView) -> Int {
        
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return logs.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "logTableCellId", for: indexPath)

        cell.textLabel?.text = logs[indexPath.row]

        return cell
    }
}

