//
//  Base.swift
//  TumblrWorker
//
//  Created by Eugene Liashenko on 10/29/15.
//  Copyright © 2015 development. All rights reserved.
//

import Foundation
import RealmSwift

struct Base {

    let realm = try! Realm()

    func blogs() -> [BlogModel] {

        let bloge = realm.objects(BlogModel).filter({ $0 != nil })
        
        return Array(bloge)
    }

    func touchBlog(_ blogName :String, date: Date?) {

        let blog = blogWithName(blogName)

        try! realm.write({

            blog.lastAnalizeDate = date

            self.realm.add(blog, update: true)
        })
    }

    func removeBlog(_ blogName :String) {

        try! realm.write({

            self.realm.delete(self.blogWithName(blogName))
        })
    }
    
    func blogWithName(_ blogName :String) -> BlogModel {
        
        guard let bloge = realm.objects(BlogModel).filter({ $0.name == blogName }).first else {
            
            let model = BlogModel()

            model.name = blogName
            
            try! realm.write({
                
                self.realm.add(model, update: true)
            })

            return model
        }

        return bloge
    }
    
    func isPresentPostWithId(_ id :Int, atBlogWith name :String) -> Bool {
        
        let bloge = blogWithName(name)
        
        if let _ = bloge.reblogedPosts.filter({ $0.id == id }).first {

            return true
        }

        return false
    }
    
    func addPostId(_ id :Int, toBlogWith name :String) {

        let bloge = blogWithName(name)

        let post = PostModel()

        post.id = id

        try! realm.write({

            bloge.reblogedPosts.append(post)
            bloge.lastAnalizeDate = Date()

            self.realm.add(bloge, update: true)
        })
    }
}
