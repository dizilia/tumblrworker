//
//  BlogTableCell.swift
//  TumblrWorker
//
//  Created by Eugene Liashenko on 11/9/15.
//  Copyright © 2015 development. All rights reserved.
//

import UIKit

class BlogTableCell :UITableViewCell {
    
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var date: UILabel!
    
}
