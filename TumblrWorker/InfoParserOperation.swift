//
//  InfoParser.swift
//  TumblrWorker
//
//  Created by Eugene Liashenko on 10/20/15.
//  Copyright © 2015 development. All rights reserved.
//

import Foundation

struct InfoParser {

    var json :Dictionary<String, AnyObject>

    init(json :Dictionary<String, AnyObject>) {

        self.json = json
    }

    func parse() -> Info {
        
        var info = Info()
        
        if let meta   = json["meta"] as? [String: AnyObject],
            let status = meta["status"] as? Int{
                
                info.status = status
                
                if let response = json["response"] as? [String: AnyObject] {
                    
                    if let user = response["user"] as? Dictionary<String, AnyObject> {
                        
                        if let name = user["name"] as? String {
                            
                            info.name = name
                        }
                        
                        if let following = user["following"] as? Int {
                            
                            info.following = following
                        }
                        
                        if let blogs = user["blogs"] as? Array<AnyObject> {
                            
                            blogs.forEach({ value in
                                
                                if let blog = value as? Dictionary<String, AnyObject> {
                                    
                                    info.blogs.append(self.parseBlog(blog))
                                }
                            })
                        }
                        
                        if let likes = user["likes"] as? Int {
                            
                            info.likes = likes
                        }
                    }
                    
                }
        }

        return info
    }
    
    func parseBlog(_ jsonBlog :Dictionary<String, AnyObject>) -> Blog {
        
        var blog = Blog()
        
        blog.admin             = jsonBlog["admin"]    as? Bool ?? false
        blog.ask               = jsonBlog["ask"]      as? Bool ?? false
        blog.ask_anon          = jsonBlog["ask_anon"] as? Bool ?? false
        blog.ask_page_title    = jsonBlog["ask_page_title"] as? String ?? ""
        blog.can_send_fan_mail = jsonBlog["can_send_fan_mail"] as? Bool ?? false
        blog.can_subscribe     = jsonBlog["can_subscribe"] as? Bool ?? false
        blog.description       = jsonBlog["description"] as? String ?? ""
        blog.drafts            = jsonBlog["drafts"] as? Int ?? 0
        blog.facebook          = jsonBlog["facebook"] as? String ?? ""
        blog.facebook_opengraph_enabled = jsonBlog["facebook_opengraph_enabled"] as? String ?? ""
        blog.followed          = jsonBlog["followed"] as? Bool ?? false
        blog.followers         = jsonBlog["followers"] as? Int ?? 0
        blog.is_blocked_from_primary = jsonBlog["is_blocked_from_primary"] as? Bool ?? false
        blog.is_nsfw           = jsonBlog["is_nsfw"] as? Bool ?? false
        blog.likes             = jsonBlog["likes"] as? Int ?? 0
        blog.messages          = jsonBlog["messages"] as? Int ?? 0
        blog.name              = jsonBlog["name"] as? String ?? ""
        blog.posts             = jsonBlog["posts"] as? Int ?? 0
        blog.primary           = jsonBlog["primary"] as? Bool ?? false
        blog.queue             = jsonBlog["queue"] as? Int ?? 0
        blog.share_likes       = jsonBlog["share_likes"] as? Bool ?? false
        blog.subscribed        = jsonBlog["subscribed"] as? Bool ?? false
        blog.title             = jsonBlog["title"] as? String ?? ""
        blog.tweet             = jsonBlog["tweet"] as? String ?? ""
        blog.twitter_enabled   = jsonBlog["twitter_enabled"] as? Bool ?? false
        blog.twitter_send      = jsonBlog["twitter_send"] as? Bool ?? false
        blog.type              = jsonBlog["type"] as? String ?? ""
        blog.updated           = jsonBlog["updated"] as? Int ?? 0
        blog.url               = jsonBlog["url"] as? String ?? ""
        
        return blog
    }
}
