//
//  Helpers.swift
//  TumblrWorker
//
//  Created by Eugene Liashenko on 10/16/15.
//  Copyright © 2015 development. All rights reserved.
//

import Foundation

struct Helpers {
    
        
    static func buildUrlString(_ baseUrlString :String, urlString :String, parameters :[String:String]) -> String {
        
        let paramsStr = parameters.reduce("", {

            return String.urlParametersCombine($0, paramsPair: $1)
        })

        return  "\(baseUrlString + urlString + paramsStr)"
    }
}

extension String {
    
    func urlencode() -> String {

        let customAllowedSet =  CharacterSet(charactersIn:"!*'();:@&=+$,/?%#[]%").inverted
        return self.addingPercentEncoding(withAllowedCharacters: customAllowedSet)!
    }
    
    static func urlParametersCombine(_ sum :String, paramsPair: (key :String, value:String)) -> String {

        return (sum == "") ? "\(paramsPair.key)=\(paramsPair.value)" : "\(sum)&\(paramsPair.key)=\(paramsPair.value)"
    }
    
    func hmacsha1(_ key: String) -> Data {
        
        let dataToDigest = self.data(using: String.Encoding.utf8)
        let secretKey    = key.data(using: String.Encoding.utf8)
        
        let digestLength = Int(CC_SHA1_DIGEST_LENGTH)
        let result = UnsafeMutablePointer<CUnsignedChar>.allocate(capacity: digestLength)
        
        CCHmac(CCHmacAlgorithm(kCCHmacAlgSHA1), (secretKey! as NSData).bytes, secretKey!.count, (dataToDigest! as NSData).bytes, dataToDigest!.count, result)
        
        return Data(bytes: UnsafePointer<UInt8>(result), count: digestLength)
        
    }
    
}

extension NSMutableData {
    
    /// Append string to NSMutableData
    ///
    /// Rather than littering my code with calls to `dataUsingEncoding` to convert strings to NSData, and then add that data to the NSMutableData, this wraps it in a nice convenient little extension to NSMutableData. This converts using UTF-8.
    ///
    /// - parameter string:       The string to be added to the `NSMutableData`.
    
    func appendString(_ string: String) {

        let data = string.data(using: String.Encoding.utf8, allowLossyConversion: true)
        append(data!)
    }
}
