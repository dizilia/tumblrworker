//
//  PostUIAction.swift
//  TumblrWorker
//
//  Created by Eugene Liashenko on 10/30/15.
//  Copyright © 2015 development. All rights reserved.
//

import UIKit
import MBProgressHUD

class PostUIAction {
    
    deinit {
        print("deinit")
    }

    func postPhotoWithData(_ data :Data, inViewController vc :UIViewController, tags :String? = .none) {

        let actionSheet = UIAlertController(title  :nil, message: nil, preferredStyle: .actionSheet)
        
        let choosePictureAction = UIAlertAction(title: "Sexy", style: .default) { (UIAlertAction) -> Void in

            self.makePhotoPostWithData(data, tags: tags ?? "girl,nice,women,hot,inspiration,fashion,ero,sexy,sport,fitness,butt,ass,tits", toBlogName: Keys.mySexyBlogName, fromVC:  vc)
        }
        
        actionSheet.addAction(choosePictureAction)
        
        let cameraAction = UIAlertAction(title: "Inspiration", style: .default){  (UIAlertAction) -> Void in

            self.makePhotoPostWithData(data, tags: "nice,inspiration,exciting,lifestyle,nature", toBlogName: Keys.myInspirationBlogName, fromVC:  vc)
        }
        
        actionSheet.addAction(cameraAction)
        
        let cancelAction = UIAlertAction(title  : "CANCEL", style  : .cancel, handler: nil)

        actionSheet.addAction(cancelAction)

        vc.present(actionSheet, animated: true) { () -> Void in }
    }
    
    func makePhotoPostWithData(_ data :Data, tags:String, toBlogName name:String, fromVC :UIViewController) {
        
        let api = API()
        
        var params = API.PostCreateParameters()
        
        params.type = .photo
        params.tags = tags
        
        api.postToBlog(name,params: params, dataParams: ["image/jpeg":data]) { (result) -> Void in
            
            print(result)
            
            guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else { return }

            switch result {
            case .done(_):
                appDelegate.vc.addLog(log: "Post Ok")
            case .error:
                appDelegate.vc.addLog(log: "Post Fails")
            }
        }

        _ = fromVC.navigationController?.popViewController(animated: true)
    }

}
