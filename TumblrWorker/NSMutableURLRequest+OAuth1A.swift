//
//  NSMutableURLRequest+OAuth1A.swift
//  TumblrWorker
//
//  Created by Eugene Liashenko on 10/19/15.
//  Copyright © 2015 development. All rights reserved.
//

import Foundation

extension NSMutableURLRequest {

    func signWithKeys(_ consumerKey:String, consumerSecret :String, token :String, tokenSecret :String, method :String = "GET",
        postParameters :[String: String]?) {

        var headerParameters = [String: String]()
        
        headerParameters["oauth_timestamp"]        = String(format: "%f", round(Date().timeIntervalSince1970))
        headerParameters["oauth_nonce"]            = ProcessInfo.processInfo.globallyUniqueString
        headerParameters["oauth_version"]          = "1.0"
        headerParameters["oauth_signature_method"] = "HMAC-SHA1"
        headerParameters["oauth_consumer_key"]     = consumerKey
        headerParameters["oauth_token"]            = token

        var signatureParameters = headerParameters

        if let query = url!.query {

            dictionaryFromQuery(query).forEach({signatureParameters.updateValue(($1), forKey: ($0))})
        }
        
        if let postParams = postParameters {
            for (k,v) in postParams {
                signatureParameters.updateValue(v, forKey: k)
            }
        }
        //
        var parameters = [String]()

        signatureParameters.keys.sorted().forEach({ parameters.append(String(format: "%@=%@", ($0).urlencode(),  signatureParameters[($0)]!.urlencode())) })
        
        let parameterString = parameters.reduce("", { return ($0) == "" ? "\($0)\($1)" :"\($0)&\($1)" })
        //

        headerParameters["oauth_signature"] = oauthSignatureForParameters(parameterString, method :method, consumerSecret :consumerSecret, tokenSecret :tokenSecret)

        //
        var components = [String]()
        
        headerParameters.forEach({ components.append("\($0)=\"\($1)\"")})

        let headerString = components.reduce("OAuth ", { return ($0) == "OAuth " ? "\($0)\($1)" : "\($0),\($1)"})
        
        self.setValue(headerString, forHTTPHeaderField: "Authorization")
    }
    
    fileprivate func oauthSignatureForParameters(_ parms :String, method: String, consumerSecret :String, tokenSecret :String) -> String {
        
        let baseURLString = url!.absoluteString.components(separatedBy: "?").first!
        
        let baseString = String(format: "%@&%@&%@", method, baseURLString.urlencode(), parms.urlencode())
        
        let sha1Digest = baseString.hmacsha1(consumerSecret + "&" + tokenSecret)
        
        let base64Encoded = sha1Digest.base64EncodedData(options: NSData.Base64EncodingOptions(rawValue: 0))

        return (NSString(data: base64Encoded, encoding: String.Encoding.utf8.rawValue) as! String).urlencode()
    }
    
    fileprivate func dictionaryFromQuery(_ query: String) -> [String:String] {

        var dict = [String:String]()

        let parameters = query.components(separatedBy: "&")
        
        parameters.forEach { (param) -> () in
            
            let keyValuePair = param.components(separatedBy: "=")
            
            if keyValuePair.count == 2 {
                
                let key   = keyValuePair[0] as String
                let value = keyValuePair[1] as String
                
                let existingValueForKey = dict[key]
                
                if existingValueForKey != nil {
                    
                    //if ([existingValueForKey isKindOfClass:[NSMutableArray class]])
                    //                    [(NSMutableArray *)existingValueForKey addObject:value];
                    //                    else
                    //                    [mutableParameterDictionary setObject:[NSMutableArray arrayWithObjects:existingValueForKey, value, nil]
                    //                    forKey:key];
                }else {
                    
                    dict[key] = value
                }
                
            }
        }
        
        return dict
    }

}
