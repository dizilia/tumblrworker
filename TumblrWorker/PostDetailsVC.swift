//
//  PostDetailsVC.swift
//  TumblrWorker
//
//  Created by Eugene Liashenko on 10/23/15.
//  Copyright © 2015 development. All rights reserved.
//

import UIKit
import Haneke
import MBProgressHUD

class PostDetailsVC :UIViewController {
    
    @IBOutlet weak var imageIV  : UIImageView!
    @IBOutlet weak var tagsLabel: UILabel!

    var post : Post!
    
    override func viewDidLoad() {

        super.viewDidLoad()

        guard let photo = post.photos.first else {

            _ = navigationController?.popViewController(animated: true)

            return
        }

        let api = API()
        
        api.likePost(postId: post.id, reblogKey: post.reblog_key)
        
        let urlString = photo.original_size.url
        
        if urlString.hasSuffix(".gif") {

            let gif = UIImage.gifWithData(try! Data(contentsOf: URL(string: urlString)!))

            imageIV.image = gif

        }else {

            let url = URL(string: photo.original_size.url)

            imageIV.hnk_setImageFromURL(url!)

            tagsLabel.text = post.tags.reduce("", {return $0! + " " + $1})
        }

        print(post.post_url)
    }

    @IBAction func didPan(_ sender: AnyObject) {
        
        _ = navigationController?.popViewController(animated: true)
    }

    @IBAction func onSave(_ sender: AnyObject) {
        
        MBProgressHUD.showAdded(to: self.view, animated: true)

        UIImageWriteToSavedPhotosAlbum(imageIV.image!, self, #selector(PostDetailsVC.image(_:didFinishSavingWithError:contextInfo:)), nil)
    }
    @IBAction func onRepost(_ sender: AnyObject) {

        guard let image = imageIV.image,
              let data  = UIImageJPEGRepresentation(image, 1) else {return}
        
        UIRouter.makePhotoPostWithData(data, inViewController: self)
    }

    func image(_ image: UIImage, didFinishSavingWithError error: NSError?, contextInfo:UnsafeRawPointer) {
        
        MBProgressHUD.hide(for: view, animated: true)
        
        if error != nil {
            
            let alert = UIAlertView()
            alert.title = "Saving"
            
            alert.message = "Error during saving image"
            alert.addButton(withTitle: "Ok")
            alert.show()
            
        }

        _ = navigationController?.popViewController(animated: true)
    }
}
