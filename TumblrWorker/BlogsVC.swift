//
//  BlogsVC.swift
//  TumblrWorker
//
//  Created by Eugene Liashenko on 11/2/15.
//  Copyright © 2015 development. All rights reserved.
//

import UIKit
import Realm

class BlogsVC :UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    @IBOutlet weak var blogNameTF: UITextField!
    @IBOutlet weak var tableView: UITableView!

    var base = Base()

    var token :RLMNotificationToken!
    
    override func viewDidLoad() {
        
        super.viewDidLoad()

        token = base.realm.observe({ [unowned self] (notification, realm) in

            self.tableView.reloadData()
        })
    }
    
    //MARK:- Actions
    @IBAction func onAddBlog(_ sender: AnyObject) {

        guard let blogName = blogNameTF.text else { return }

        base.touchBlog(blogName, date: Date())
    }

    //MARK:- Table
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {

        return base.blogs().count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "BlogTableCell", for: indexPath) as! BlogTableCell

        let blog = base.blogs()[indexPath.row]

        cell.name.text = blog.name

        let date = blog.lastAnalizeDate ?? Date(timeIntervalSince1970: 0)

        let dateFormatter = DateFormatter()
        
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
     //   dateFormatter.timeZone = NSTimeZone(name: "UTC")

        cell.date.text = dateFormatter.string(from: date)

        return cell
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {

        UIRouter.showBlogAnalizeWithParant(navigationController!, blogName:base.blogs()[indexPath.row].name)
    }
}
