//
//  PostsParserOperation.swift
//  TumblrWorker
//
//  Created by Eugene Liashenko on 10/20/15.
//  Copyright © 2015 development. All rights reserved.
//

import Foundation
import Haneke

class PostsParserOperation :ParserOperation {

    var didFinish :((_ result :NetworkOperationResult<[Post]>)->Void)?

    init(didFinish :((_ result :NetworkOperationResult<[Post]>)->Void)?) {

        self.didFinish = didFinish
    }

    init(data: Data, didFinish :((_ result :NetworkOperationResult<[Post]>)->Void)?) {
        
        super.init()

        self.didFinish = didFinish
        
        guard let json1 = JSON.convertFromData(data) else { return }

        json = json1.dictionary
    }
    
    override func parse() {
        
        if let meta   = json["meta"] as? [String: AnyObject],
           let status = meta["status"] as? Int{

                if status != 200 {

                    didFinish?(NetworkOperationResult<[Post]>.error)
                    return
                }
        }

        var result = [Post]()
        
        if let response = json["response"] as? [String: AnyObject] {
        
            if let posts = response["posts"] as? Array<Dictionary<String, AnyObject>> {
                
                posts.forEach({ result.append(PostsParserOperation.parsePostFromJson($0)) })
                
            }
        }
        
        didFinish?(NetworkOperationResult<[Post]>.done(result))
    }

    class func parsePostFromJson(_ json :Dictionary<String, AnyObject>) -> Post {
        
        var post = Post()
        
        post.liked           = json["liked"]           as? Bool     ?? false
        post.blog_name       = json["blog_name"]       as? String   ?? ""
        post.can_reply       = json["can_reply"]       as? Bool     ?? false
        post.caption         = json["caption"]         as? String   ?? ""
        post.date            = json["date"]            as? String   ?? ""
        post.followed        = json["followed"]        as? Bool     ?? false
        post.format          = json["format"]          as? String   ?? ""
        post.id              = json["id"]              as? Int      ?? 0
        post.image_permalink = json["image_permalink"] as? String   ?? ""
        post.liked           = json["liked"]           as? Bool     ?? false
        post.note_count      = json["note_count"]      as? Int      ?? 0
        post.post_url        = json["post_url"]        as? String   ?? ""
        post.reblog_key      = json["reblog_key"]      as? String   ?? ""
        post.short_url       = json["short_url"]       as? String   ?? ""
        post.slug            = json["slug"]            as? String   ?? ""
        post.state           = json["state"]           as? String   ?? ""
        post.timestamp       = json["timestamp"]       as? Int      ?? 0
        post.type            = json["type"]            as? String   ?? ""
        post.tags            = json["tags"]            as? [String] ?? ["UNKNOWN"]

        let op = PostsParserOperation(didFinish: nil)
        
        post.photos = op.parsePhotosFromJson(json)

        if let reblog = json["reblog"] as? Dictionary<String, AnyObject> {

            var tt = PostReblog()

            tt.tree_html = reblog["tree_html"] as? String ?? ""
            tt.comment   = reblog["comment"]   as? String ?? ""

            post.reblog = tt
        }

        if let highlighted = json["highlighted"] as? [AnyObject] {
            
            if highlighted.count > 0 {
                
                print("HIGLITED")
            }
            
            //post.highlighted
        }

        if let trail = json["trail"] as? Array<Dictionary<String, AnyObject>> {
            
            trail.forEach({

                post.trail.append(op.parseTrailFromJson($0))
            })
        }
        
        post.notes = op.parseNotesFromJson(json)

        return post
    }
    
    func parseNotesFromJson(_ json :Dictionary<String, AnyObject>) -> [PostNote] {
        
        var result = [PostNote]()

        if let notes = json["notes"] as? Array<Dictionary<String, AnyObject>> {

            notes.forEach({

                var note = PostNote()

                note.blog_name = ($0)["blog_name"] as? String ?? ""
                note.blog_url  = ($0)["blog_url"]  as? String ?? ""
                note.blog_uuid = ($0)["blog_uuid"] as? String ?? ""
                note.timestamp = ($0)["timestamp"] as? String ?? ""
                note.type      = ($0)["type"]      as? String ?? ""

                result.append(note)
            })
        }

        return result
    }
    
    func parseTrailFromJson(_ json :Dictionary<String, AnyObject>) -> PostTrail {
        
        var result = PostTrail()

        result.content_raw  = json["content_raw"]  as? String ?? ""
        result.content      = json["content"]      as? String ?? ""
        result.is_root_item = json["is_root_item"] as? Bool   ?? false
        
        if let post = json["post"] as? Dictionary<String, AnyObject> {
            
            result.postId = post["id"] as? String ?? ""
        }
        
        if let blog = json["blog"] as? Dictionary<String, AnyObject> {
            
            var postBlog = PostBlog()
            
            postBlog.active = blog["active"] as? Bool   ?? false
            postBlog.name   = blog["name"]   as? String ?? ""
            
            if let theme = json["theme"] as? Dictionary<String, AnyObject> {
                
                var postTheme = PostBlog.Theme()
                
                postTheme.avatar_shape         = theme["avatar_shape"]         as? String ?? ""
                postTheme.background_color     = theme["background_color"]     as? String ?? ""
                postTheme.body_font            = theme["body_font"]            as? String ?? ""
                postTheme.header_bounds        = theme["header_bounds"]        as? String ?? ""
                postTheme.header_focus_height  = theme["header_focus_height"]  as? Int    ?? 0
                postTheme.header_focus_width   = theme["header_focus_width"]   as? Int    ?? 0
                postTheme.header_full_height   = theme["header_full_height"]   as? Int    ?? 0
                postTheme.header_full_width    = theme["header_full_width"]    as? Int    ?? 0
                postTheme.header_image         = theme["header_image"]         as? String ?? ""
                postTheme.header_image_focused = theme["header_image_focused"] as? String ?? ""
                postTheme.header_image_scaled  = theme["header_image_scaled"]  as? String ?? ""
                postTheme.header_stretch       = theme["header_stretch"]       as? Bool   ?? false
                postTheme.link_color           = theme["link_color"]           as? String ?? ""
                postTheme.show_avatar          = theme["show_avatar"]          as? Bool   ?? false
                postTheme.show_description     = theme["show_description"]     as? Bool   ?? false
                postTheme.show_header_image    = theme["show_header_image"]    as? Bool   ?? false
                postTheme.show_title           = theme["show_title"]           as? Bool   ?? false
                postTheme.title_color          = theme["title_color"]          as? String ?? ""
                postTheme.title_font           = theme["title_font"]           as? String ?? ""
                postTheme.title_font_weight    = theme["title_font_weight"]    as? String ?? ""
                
                postBlog.theme = postTheme
            }
            
            result.blog = postBlog
        }
        
        return result
    }
    
    func parsePhotosFromJson(_ json :Dictionary<String, AnyObject>) -> [PostPhoto] {
        
        var result = [PostPhoto]()
        
        if let photos = json["photos"] as? Array<AnyObject> {
            
            photos.forEach({
                
                if let photo = $0 as? Dictionary<String, AnyObject> {
                    
                    var postPhoto = PostPhoto()
                    
                    postPhoto.caption = photo["caption"] as? String ?? ""
                    
                    if let photoSizeJson = photo["original_size"] as? Dictionary<String, AnyObject> {
                        
                        var photoSize = PostPhotoSize()
                        
                        photoSize.url    = photoSizeJson["url"]    as? String ?? ""
                        photoSize.height = photoSizeJson["height"] as? Int    ?? 0
                        photoSize.width  = photoSizeJson["width"]  as? Int    ?? 0

                        postPhoto.original_size = photoSize
                    }

                    if let altSizesJson = photo["alt_sizes"] as? Array<[String: AnyObject]> {

                        var altSizes = [PostPhotoSize]()

                        altSizesJson.forEach({

                            var photoSize = PostPhotoSize()

                            photoSize.url    = ($0)["url"]    as? String ?? ""
                            photoSize.height = ($0)["height"] as? Int    ?? 0
                            photoSize.width  = ($0)["width"]  as? Int    ?? 0

                            altSizes.append(photoSize)
                        })

                        postPhoto.alt_sizes = altSizes
                    }

                    result.append(postPhoto)
                }
            })
        }

        return result
    }
}
