//
//  Post.swift
//  TumblrWorker
//
//  Created by Eugene Liashenko on 10/20/15.
//  Copyright © 2015 development. All rights reserved.
//

import Foundation

public struct Post {
    
    public var liked           = false
    public var state           = ""
    public var format          = ""
    public var can_reply       = false
    public var date            = ""
    public var post_url        = ""
    public var followed        = false
    public var timestamp       = 0
    public var note_count      = 0
   // public var recommended_color =
    public var reblog          = PostReblog()
    public var image_permalink = ""
    public var type            = ""
    public var tags            = [String]()
    public var trail           = [PostTrail]()
    public var id              = 0
    public var short_url       = ""
    public var reblog_key      = ""
    public var caption         = ""
    public var photos          = [PostPhoto]()
    public var blog_name       = ""
//    public var recommended_source =
    public var highlighted     = [String]()
    public var slug            = ""
    public var notes           = [PostNote]()
}

public struct PostNote {
    
    public var timestamp = ""
    public var blog_uuid = ""
    public var blog_url  = ""
    public var blog_name = ""
    public var type      = ""
}

public struct PostReblog {
 
    public var tree_html = ""
    public var comment   = ""
}

public struct PostPhoto {
    
    public var caption       = ""
    public var original_size = PostPhotoSize()
    public var alt_sizes     = [PostPhotoSize]()
}

public struct PostPhotoSize {
    
    public var url    = ""
    public var width  = 0
    public var height = 0
}

public struct PostTrail {
    
    public var postId       = ""
    public var content_raw  = ""
    public var content      = ""
    public var is_root_item = false
    public var blog         = PostBlog()
}

public struct PostBlog {
    
    public struct Theme {
        
        public var title_font_weight    = ""
        public var header_image_scaled  = ""
        public var header_focus_width   = 0
        public var header_bounds        = ""
        public var show_description     = false
        public var avatar_shape         = ""
        public var link_color           = ""
        public var header_image         = ""
        public var header_full_height   = 0
        public var header_stretch       = false
        public var background_color     = ""
        public var header_focus_height  = 0
        public var show_avatar          = false
        public var show_title           = false
        public var body_font            = ""
        public var title_color          = ""
        public var show_header_image    = false
        public var title_font           = ""
        public var header_full_width    = 0
        public var header_image_focused = ""
    }
    
    public var name   = ""
    public var theme  = Theme()
    public var active = false
}