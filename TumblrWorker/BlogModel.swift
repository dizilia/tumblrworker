//
//  BlogModel.swift
//  TumblrWorker
//
//  Created by Eugene Liashenko on 10/29/15.
//  Copyright © 2015 development. All rights reserved.
//

import RealmSwift

class BlogModel: Object {

    dynamic var name = ""
    dynamic var lastAnalizeDate : Date?

    let reblogedPosts = List<PostModel>()
    
    override static func primaryKey() -> String? {

        return "name"
    }
}

class PostModel: Object {

    dynamic var id = 0
}
