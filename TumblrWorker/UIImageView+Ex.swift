//
//  UIImageView+Ex.swift
//  TumblrWorker
//
//  Created by Eugene Liashenko on 10/24/15.
//  Copyright © 2015 development. All rights reserved.
//

import UIKit

extension UIImageView {
    
    func loadImageFromUrl(_ urlString :String?, placeholder :UIImage? = nil) {
        
        image = placeholder
        
        guard let urlStr = urlString,
              let url    = URL(string: urlStr) else {return}

        let queue = DispatchQueue.global(priority: DispatchQueue.GlobalQueuePriority.high)

        queue.async { () -> Void in
            
            guard let imageData = try? Data(contentsOf: url) else {return}
            
            DispatchQueue.main.async(execute: {[weak self] () -> Void in

                self?.image = UIImage(data: imageData)
            })
        }
    }
}
