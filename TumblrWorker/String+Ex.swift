//
//  String+Ex.swift
//  TumblrWorker
//
//  Created by Eugene Liashenko on 10/29/15.
//  Copyright © 2015 development. All rights reserved.
//

import Foundation

extension String {
    
    func toDate() -> Date? {
        
        let dateFormatter = DateFormatter()

        let ramge = self.range(of: " GMT")
        
        let dateStr = self.substring(to: (ramge?.lowerBound)!)

        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        dateFormatter.timeZone = TimeZone(identifier: "UTC")
        
        return dateFormatter.date(from: dateStr)
    }
}
