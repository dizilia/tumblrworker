//
//  AnalizerVC.swift
//  TumblrWorker
//
//  Created by Eugene Liashenko on 10/23/15.
//  Copyright © 2015 development. All rights reserved.
//

import UIKit
import Haneke

class AnalizerVC :UIViewController,
UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout{
    
    @IBOutlet weak var label         : UILabel!
    @IBOutlet weak var lastDate      : UILabel!
    @IBOutlet weak var collectionView: UICollectionView!
    

    var api   = API()
    var posts = [Post]()

    var askedCount      = 0
    var currentBlogName = ""

    var base = Base()
    
    override func viewDidLoad() {
        
        super.viewDidLoad()

        applyNewBlogToAnalize(currentBlogName.isEmpty ? "linxspiration.com" : currentBlogName)
    }

    func applyNewBlogToAnalize(_ blogName :String) {
        
        currentBlogName = blogName
        
        askedCount = 0

        posts.removeAll()

        navigationItem.title = currentBlogName

        onRun(self)
    }
    
    //MARK:-
    @IBAction func onRun(_ sender: AnyObject) {
        
        var params = API.PostRequestParameters()
        
       // params.notes_info  = true
       // params.reblog_info = true
        params.limit   = 50
        params.offset  = askedCount
        
        api.getPostsForBlogName(currentBlogName, params :params) {[weak self] (result) -> Void in
            
            switch result {
            case .done(let newPosts):

                self?.applyNewPosts(newPosts)

            case .error:
                
                print("error")
            }
        }

    }
    
    @IBAction func onBack(_ sender: AnyObject) {

        _ = navigationController?.popViewController(animated: true)
    }
    @IBAction func onRemoveBlog(_ sender: AnyObject) {

        base.removeBlog(currentBlogName)

        onBack(self)
    }

    //MARK:-
    func applyNewPosts(_ newPosts :[Post]) {

        if newPosts.isEmpty {
            return
        }

        askedCount += newPosts.count

        let lastBlogedate = base.blogWithName(self.currentBlogName).lastAnalizeDate ?? Date()

        lastDate.text = "\(lastBlogedate)"

        posts += newPosts.sorted(by: { return $0.note_count > $1.note_count})

        let lastPost = newPosts.last

        label.text = lastPost?.date

        collectionView.reloadData()
        collectionView.scrollToItem(at: IndexPath(row: posts.count - newPosts.count, section: 0), at: UICollectionViewScrollPosition.bottom, animated: true)
    }
    
    //MARK:- Collection
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return posts.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "PostCollectionCell", for: indexPath) as! PostCollectionCell

        if indexPath.row >= posts.count {return cell}
        
        let post = posts[indexPath.row]
        
        cell.textLabel.text = String(post.note_count)

        guard let photo = post.photos.first else {return cell}
        
        cell.backgroundColor = base.isPresentPostWithId(post.id, atBlogWith: currentBlogName) ? UIColor.green : UIColor.black
        
        let sorted = photo.alt_sizes.sorted(by: {return ($0).width < ($1).width})
        
        if  let urlStr = sorted.first?.url,
            let imageUrl = URL(string: urlStr) {

                cell.imageView.image = nil
                cell.imageView.hnk_setImageFromURL(imageUrl)
        }

        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        let vc = storyboard?.instantiateViewController(withIdentifier: "PostDetailsVC") as! PostDetailsVC
        
        let post = posts[indexPath.row]
        
        vc.post = post
        
        base.addPostId(post.id, toBlogWith: currentBlogName)
        
        navigationController?.pushViewController(vc, animated: true)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {

        return CGSize(width: view.bounds.size.width / 2, height: 100)
    }
    
}
