//
//  PostParserOperation.swift
//  TumblrWorker
//
//  Created by Eugene Liashenko on 10/24/15.
//  Copyright © 2015 development. All rights reserved.
//

import Foundation

class PostParserOperation :ParserOperation {
    
    var didFinish :((_ result :NetworkOperationResult<Bool>)->Void)?
    
    init(didFinish :((_ result :NetworkOperationResult<Bool>)->Void)?) {
        
        self.didFinish = didFinish
    }
    
    override func parse() {
        
        var result = false
        
        if let meta   = json["meta"] as? [String: AnyObject],
           let status = meta["status"] as? Int{
                
            result = (status == 201)
        }

        DispatchQueue.main.async {[weak self] () -> Void in

            self?.didFinish?(NetworkOperationResult<Bool>.done(result))
        }
        
        print(json)
    }
}
