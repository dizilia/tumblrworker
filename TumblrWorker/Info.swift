//
//  Info.swift
//  TumblrWorker
//
//  Created by Eugene Liashenko on 10/20/15.
//  Copyright © 2015 development. All rights reserved.
//

import Foundation

public struct Info {
    
    public var name      = ""
    public var following = 0
    public var likes     = 0
    public var status    = 401
    
    public var blogs = [Blog]()

}

public struct Blog {
    
    public var title           = ""
    public var subscribed      = false
    public var description     = ""
    public var followed        = false
    public var primary         = false
    public var ask             = false
    public var followers       = 0
    public var drafts          = 0
    public var updated         = 0
    public var name            = ""
    public var type            = ""
    public var posts           = 0
    public var likes           = 0
    public var tweet           = ""
    public var share_likes     = false
    public var url             = ""
    public var ask_anon        = false
    public var twitter_send    = false
    public var ask_page_title  = ""
    public var admin           = false
    public var twitter_enabled = false
    public var messages        = 0
    public var can_subscribe   = false
    public var is_nsfw         = false
    public var queue           = 0
    public var facebook        = ""
    public var facebook_opengraph_enabled = ""
    public var can_send_fan_mail          = false
    public var is_blocked_from_primary    = false
}